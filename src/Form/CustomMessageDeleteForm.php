<?php

namespace Drupal\custom_messages\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Custom Message entities.
 *
 * @ingroup custom_messages
 */
class CustomMessageDeleteForm extends ContentEntityDeleteForm {


}
