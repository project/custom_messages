<?php

namespace Drupal\custom_messages;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for custom_message.
 */
class CustomMessageTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.

}
